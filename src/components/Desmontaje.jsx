import React, { Component } from 'react';

class ComponenteDesmontaje extends Component {
  state = {
    windowWidth: 0
  }
  componentDidMount(){
    window.addEventListener('resize', this.actualizarEstadoAncho);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.actualizarEstadoAncho)
  }
  actualizarEstadoAncho = ()=>{
    console.log('ejecutando evento');
    this.setState({
      windowWidth: document.body.clientWidth
    })
  }
  render() {
    return (
      <div>
        <p>El ancho de la pantalla es: {
          this.state.windowWidth}</p>
      </div>
    )
  }
}

class Desmontaje extends Component {
  state = {
    mostrarComponente: true
  }
  render() {
    if(this.state.mostrarComponente) {
      return (
        <div>
          <h1>Fase de desmontaje</h1>
          <ComponenteDesmontaje/>
          <button 
            onClick={()=> this.setState({mostrarComponente: false})}>
              Desmontar Componente</button>
        </div>
      );
    }

    return (
      <p>El componente esta desmontado</p>
    )
    
  }
}

export default Desmontaje;