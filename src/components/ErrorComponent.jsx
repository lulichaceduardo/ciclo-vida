import React, { Component } from 'react';

class BotonLanzaError extends Component {
  state = {
    elError: false
  }
  render() {
    if(this.state.elError){
      throw new Error('Error lanzado por el boton');
    }
    return(
      <button onClick= {()=>this.setState({elError: true})}>GENERA ERROR</button>
    )
  }
}

class ErrorComponent extends Component {
  state = {
    hasError: false,
    errorMsg: ''
  }
  componentDidCatch(error, errorInfo){
    console.log('-> componentDidCatch');
    console.log({error, errorInfo});
    this.setState({
      hasError: true,
      errorMsg: error.toString()
    })

  }
  render() {
    if(this.state.hasError) {
      return(
        <div>
          <p>Error en el componente: {this.state.errorMsg}</p>
          <button
            onClick={()=> this.setState({hasError: false})}
          >Volver a la aplicacion</button>
        </div>
      )
    }
    return (
      <div>
        <h1>Fase de error</h1>
        <BotonLanzaError/>
      </div>
    );
  }
}

export default ErrorComponent;