import React, { Component } from 'react';

class Montaje extends Component {
  constructor(props){
    super(props);
    
    console.log('-> constructor');
    this.state = {
      title: 'Titulo Inicial',
      bpi: ''
    }
    // this.handleClick = this.handleClick.bind(this);
  }
  UNSAFE_componentWillMount() {
    
    console.log('-> componentWillMount');
    this.setState({title: 'titulo actualizado en el componentWillMount'})
  }

  componentDidMount() {
    fetch('https://api.coindesk.com/v1/bpi/currentprice.json')
    .then((res)=> res.json())
    .then((data)=>{
      const {bpi} = data
      this.setState({bpi})
    })
    console.log('-> componentDidMount');
  }
  handleClick = () => this.setState({title: 'titulo cambiado'});
  // handleClick() {
  //   console.log('diste click');
  //   this.setState({title: 'titulo cambiado'});
  // }
  render() {
    const {bpi} = this.state
    const currentList = Object.keys(bpi);
    
    console.log(currentList);
    return (
      <div className="App">
        <h1>Ciclos de vida en react</h1>
        <h4>{this.state.title}</h4>
        <button onClick={this.handleClick}>cambiar titulo</button>
        <h2>Conversion de Bitcoin a otras monedas</h2>
        {currentList.map((current)=>{
          return(
            <p key={current}>1 BITCOIN equivale a {bpi[current].rate} {current}</p>
          )
        })}
        
      </div>
    );
  }
}

export default Montaje;