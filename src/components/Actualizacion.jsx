import React, { Component } from 'react';

const ANIMAL_IMAGES = {
  panda: 'https://goo.gl/oNbtoq',
  gato: 'https://goo.gl/PoQQXb',
  delfin: 'https://goo.gl/BbiKCd'
}

class AnimalImage extends Component{
  constructor(props){
    super(props);
    this.state = {
      src: ANIMAL_IMAGES[this.props.animal]
    }
  }
  // UNSAFE_componentWillReceiveProps(nextProps){
  //   console.log('-> componentWillReceiveProps');
  //   // console.log(this.props.animal);
  //   // console.log(nextProps);
  //   if(this.props.animal !== nextProps.animal){
  //     console.log('actualizando el estado')
  //     this.setState({src: ANIMAL_IMAGES[nextProps.animal]})
  //   }
  // }

  static getDerivedStateFromProps(props, state) {
    console.log('-> getDerivedStateFromProps');
    return {
      src: ANIMAL_IMAGES[props.animal]
    };
  }

  shouldComponentUpdate(nextProps, nextState){
    console.log('-> shouldComponentUpdate');
    return this.props.animal !== nextProps.animal
  }

  // UNSAFE_componentWillUpdate(nextProps, nextState){
  //   console.log('-> componentWillUpdate');
  //   const img = document.querySelector('img');
  //   img.animate([
  //     {filter: 'blur(0px)'},
  //     {filter: 'blur(2px)'}
  //   ], {duration: 500, easing: 'ease'})
  // }

  getSnapshotBeforeUpdate(prevProps, prevState){
    console.log('-> getSnapshotBeforeUpdate')
      const img = document.querySelector('img');
      img.animate([
        {filter: 'blur(0px)'},
        {filter: 'blur(2px)'}
      ], {duration: 500, easing: 'ease'})
      const reverFilter = {
        start: 'blur(2px)', finish: 'blur(0px)'
      }
    return reverFilter;
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    console.log('-> componentDidUpdate');
    console.log(snapshot);
    const img = document.querySelector('img');
    img.animate([
      {filter: snapshot.start},
      {filter: snapshot.finish}
    ], {duration: 3500, easing: 'ease'})
  }

  render() {
    console.log('-> render');
    return(
      <div>
        <p>El animal es: {this.props.animal}</p>
      <img
        style={{width: '250px'}} 
        src={this.state.src} 
        alt={this.props.animal}/>
      </div>
      
    )
  }
}

class Actualizacion extends Component {
  state = {
    animal: 'panda'
  }
  render() {
    return (
      <div>
        <h1>Fase de Actualizacion</h1>
        <button onClick={()=> this.setState({animal: 'panda'})}>panda</button>
        <button onClick={()=> this.setState({animal: 'gato'})}>gato</button>
        <button onClick={()=> this.setState({animal: 'delfin'})}>delfin</button>
        <AnimalImage animal={this.state.animal}/>
      </div>
    );
  }
}

export default Actualizacion;